import java.util.Scanner;


public class VirtualPetApp {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// Owl o1 = new Owl(colour, species, faceShape)
		
		Owl o1 = new Owl("Black", "Snowy", "round"); 
		Owl o2 = new Owl("Brown", "Sooty", "heart");
	
	
		//System.out.println(o3.getType(o3.faceShape, o3.species));
		
		Owl[] parliament = new Owl[4];
		
		for(int i = 0; i < parliament.length; i++) {
				System.out.println("	Owl:" + (i + 1));
				parliament[i] = new Owl("","","");
				System.out.println("Colour:");
				parliament[i].colour = reader.nextLine();
				System.out.println("Species:");
				parliament[i].species = reader.nextLine();
				System.out.println("Face Shape:");
				parliament[i].faceShape = reader.nextLine();
		}
		
		//OWL 4'S ATTRIBUTES
		System.out.println(parliament[3].colour);
		System.out.println(parliament[3].species);
		System.out.println(parliament[3].faceShape);
		
		
		//OWL 1 getType()
		System.out.println(parliament[0].getType(parliament[0].faceShape, parliament[0].species));
		
		
		// ANIMAL INSTANCE
		
		System.out.println(o1.fly(o1.colour,o1.species));
		System.out.println(o2.getType(o2.faceShape, o2.species));
		
	}
}